################################# TradeManagement using RabbitMQ,Spring, hibernate to develop RestAPI MicroService ###############
This Project Build with  SpringBoot used frameWoek and Third Party API.  

1)JPA  
2)JMS --> RABBITMQ  
3)Database Spring data using hibernate connecting --> MYSQL  
4) Log4j has been used for logging.  

       ######################   JPA ################################  
 Using CrudRepository for handling DB Table CURD Operation.  
CrudRepository provide NAMED Query and Handling select Query .  
Select Query By Method Name and criteria usign AND clause java property name.  

######################   JMS ################################  
Using JMS for send message to Client ..  
TopicExchanger for Working as BRoker.  
RAbbitMQ is the third party implemention for JMS.  
Here multiple Queue connect to to TOPIC Exchanger.
We are creating two Queue with one "Direct" exchanger. e.g. of Queue is home.appliance.prd  

######################   DataBase connection ################################  
  
Hibernate used as ORM. We are using Crudrepository of Spring data to doing the db operations.  
property file comprises the mysql and hibernate configuration.   
  

######################   Example End Point ################################  

Example of one end point using home.appliance.prd Queue  

POSTRequest http://localhost:<portnum>/order/placeorder  
{"name":"bed",
 "description":"This is Tempur bed",
 "category": "Clothing",
 "price":190.00,
 "orderType":"home.appliance.prd"
}  
ResponseWithTicketNumber  
{
    "name": null,
    "description": null,
    "category": null,
    "price": null,
    "ticketNumber": "58d2034f-4891-4b50-8e6f-60cc907e72bb",
    "ticketCreationTime": null
}  
