package com.krish.tms.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.krish.tms.controller.AuthenticationController;
import com.krish.tms.dto.UserDTO;
import com.krish.tms.service.AuthenticationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@WebMvcTest
public class ApplicationTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthenticationService authenticationService;

    @MockBean
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private AuthenticationController authenticationController;


    @Test
    public void contextLoads() throws Exception {
        assertThat(authenticationController).isNotNull();
    }
    @Test
    public void shouldReturnDefaultMessage() throws Exception {
      // when(authenticationService.()).thenReturn("Hello Mock");
        this.mockMvc.perform(get("/hello")).andDo(print()).andExpect(content().string(containsString("Hello TMS System")));

    }

    @Test
    public void shouldReturnCreatedUser() throws Exception {
        // when(authenticationService.()).thenReturn("Hello Mock");
        UserDTO userDTO = new UserDTO();

        userDTO.setName("shyam");
        userDTO.setEmailId("shyam@gmaildn.com");
        userDTO.setAge(25);
        when(authenticationService.create(userDTO)).thenReturn(userDTO);
        this.mockMvc.perform(post("/login/createuser")
                .content(asJsonString(userDTO))
                 .contentType(MediaType.APPLICATION_JSON)
                  .accept(MediaType.APPLICATION_JSON));

    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
