package com.krish.tms.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.krish.tms.messages.FruitMessagesListener;
import com.krish.tms.messages.HomeApplianceMessagesListener;
import com.krish.tms.utils.Constants;

@Configuration 
public class RabbitMQConfig {
	
	private static Queue q1 = null;
	private static Queue q2 = null;
	
    @Bean
    public Queue homeApplianceQueue(){
        System.out.println("homeApplianceQueue");
        q1 = new Queue(Constants.TMS_HOME_APPLIANCE_QUEUE, true);
        return q1;
    }
    
    @Bean
    public Queue fruitQueue(){
        System.out.println("fruitQueue");
        q2 = new Queue(Constants.TMS_FRUIT_QUEUE, true);
        return q2 ;  
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange("TMS_TOPIC_EXCHANGER");
    }

    @Bean
    Binding homeApplianceBinding(TopicExchange exchange) {
        return BindingBuilder.bind(q1).to(exchange).with(Constants.TMS_HOME_APPLIANCE_QUEUE);
    }
    
    @Bean
    Binding fruitBinding(TopicExchange exchange) {
        return BindingBuilder.bind(q2).to(exchange).with(Constants.TMS_FRUIT_QUEUE);
    }

    @Bean("homeMLA")
    MessageListenerAdapter homeApplianceMessagesListener(HomeApplianceMessagesListener homeApplianceMessagesListener) {
        return new MessageListenerAdapter(homeApplianceMessagesListener, "receiveMessage");
    }

    @Bean("frtMLA")
    MessageListenerAdapter fruitMessagesListener(FruitMessagesListener fruitMessagesListener) {
        return new MessageListenerAdapter(fruitMessagesListener, "receiveMessage");
    }
    
    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
    		 @Qualifier("homeMLA") MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(Constants.TMS_HOME_APPLIANCE_QUEUE);
        container.setMessageListener(listenerAdapter);
        return container;
    }
    
    @Bean
    SimpleMessageListenerContainer container1(ConnectionFactory connectionFactory,
    		@Qualifier("frtMLA")  MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(Constants.TMS_FRUIT_QUEUE);
        container.setMessageListener(listenerAdapter);
        return container;
    }
    
    
}
