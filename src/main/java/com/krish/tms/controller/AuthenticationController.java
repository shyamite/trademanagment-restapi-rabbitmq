package com.krish.tms.controller;

import com.krish.tms.dto.UserDTO;
import com.krish.tms.service.IAuthenticationService;
import com.krish.tms.service.OrderService;
import org.apache.log4j.Logger;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.JsonPath;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;


@RestController
public class AuthenticationController {

    private static final Logger logger = Logger.getLogger(AuthenticationController.class);

    @Autowired
    IAuthenticationService authenticationService;

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/login/validate", method = RequestMethod.POST)
    public UserDTO validateUser(@RequestBody UserDTO user) {
        logger.info("++validateUser");
        UserDTO isValidUser = authenticationService.validateUser(user);
        logger.info("--validateUser");
        return isValidUser;
    }

    @RequestMapping(value = "/login/createuser", method = RequestMethod.POST)
    public UserDTO createUser(@RequestBody UserDTO user) {
        logger.info("++createuser");
        UserDTO isValidUser = authenticationService.create(user);
        logger.info("--createuser");
        return isValidUser;
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String testingHello() {
        logger.info("++testingHello");
        logger.info("--testingHello");
        return "Hello TMS System";
    }


    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public String validateUserForQue(@PathParam("msg") String msg) {
        logger.info("++validateUserForQue");

      //  orderService.sendMessage(msg);
        logger.info("--validateUserForQue");
        return "Hello TMS System";
    }

}
