package com.krish.tms.controller;

import com.krish.tms.dto.OrderDTO;
import com.krish.tms.dto.OrderRespDTO;
import com.krish.tms.service.OrderService;
import com.krish.tms.utils.Constants;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    private static final Logger logger = Logger.getLogger(OrderController.class);

    @RequestMapping(value = "/order/placeorder", method = RequestMethod.POST)
    public OrderRespDTO placeOrder(@RequestBody OrderDTO orderDTO) {
        logger.info("++placeOrder");
        OrderRespDTO orderResponseDTO=null;
        //for(int i=0 ; i < 10000 ; i++) {
            orderResponseDTO = orderService.placeOrder(orderDTO);
      //  }
        logger.info("--placeOrder");
        return orderResponseDTO;
    }
    
       @RequestMapping(value = "/order/test", method = RequestMethod.GET)
    public OrderRespDTO placeOrder() {
        logger.info("++placeOrder");
        OrderRespDTO orderResponseDTO=null;
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setName("APPLE");
        orderDTO.setOrderType(Constants.TMS_FRUIT_QUEUE);
        orderService.placeOrder(orderDTO);
        
        OrderDTO orderDTO1 = new OrderDTO();
        orderDTO1.setName("TVVVVV");
        orderDTO1.setDescription("This is smart tv");
        orderDTO1.setOrderType(Constants.TMS_HOME_APPLIANCE_QUEUE);
        orderService.placeOrder(orderDTO1);
        
        logger.info("--placeOrder");
        return orderResponseDTO;
    } 
	

}
