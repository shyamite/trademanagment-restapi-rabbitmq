package com.krish.tms.entity;

import com.krish.tms.dto.OrderDTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity

public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @javax.persistence.Id
    @GeneratedValue
    private Long Id;

    private String name;
    private String description;
    private String category;
    private BigDecimal price;


    public  Product(){}
    public  Product(OrderDTO orderDTO){
        this.name =orderDTO.getName();
        this.description=orderDTO.getDescription();
        this.category=orderDTO.getCategory();
        this.price=orderDTO.getPrice();
    }



    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


}

