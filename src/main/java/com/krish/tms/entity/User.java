package com.krish.tms.entity;


import com.krish.tms.dto.UserDTO;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class User implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long Id;

    private String name;
    private String emailId;
    private int age;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private Set<Address> addresses;

    @Column(nullable = false)
    @CreationTimestamp
    private Timestamp createTime = new Timestamp(GregorianCalendar.getInstance().getTimeInMillis());


    @Column(nullable = false)
    @UpdateTimestamp
    private Timestamp editTime = new Timestamp(GregorianCalendar.getInstance().getTimeInMillis());


    public User(UserDTO userdto) {
        this.name = userdto.getName();
        this.emailId = userdto.getEmailId();

        Set<Address> myAddress = new HashSet<Address>();
        if(userdto.getAddresses() != null) {
            myAddress = userdto.getAddresses().stream().map(addressDTO ->
                    new Address(addressDTO)
            ).collect(Collectors.toSet());
        }
        this.addresses = myAddress;
        this.age = userdto.getAge();
    }


    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return "User{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", emailId='" + emailId + '\'' +
                ", age=" + age +
                ", addresses=" + addresses +
                ", createTime=" + createTime +
                ", editTime=" + editTime +
                '}';
    }
}
