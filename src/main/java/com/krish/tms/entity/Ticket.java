package com.krish.tms.entity;

import com.krish.tms.dto.OrderDTO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

@Entity

public class Ticket implements Serializable {

    private static final long serialVersionUID = 1L;

    @javax.persistence.Id
    @GeneratedValue
    private Long Id;
	private String ticketId;
	private String ticketIdProduct;

	private Long productId;


	public String getTicketIdProduct() {
		return ticketIdProduct;
	}

	public void setTicketIdProduct(String ticketIdProduct) {
		this.ticketIdProduct = ticketIdProduct;
	}

	public String getTicketId() {
		UUID uuid = UUID.randomUUID();
		String randomUUIDString = uuid.toString();
		System.out.println("UUID=" + randomUUIDString );
		return randomUUIDString;
	}

	public Long getId() {

		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "Ticket{" +
				"Id=" + Id +
				", ticketId='" + ticketId + '\'' +
				", ticketIdProduct='" + ticketIdProduct + '\'' +
				", productId=" + productId +
				'}';
	}
}

