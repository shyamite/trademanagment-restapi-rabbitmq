package com.krish.tms.messages;

import com.krish.tms.dto.OrderDTO;
import com.krish.tms.entity.Product;
import com.krish.tms.repository.IProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class OrderMessagesListener {

    @Autowired
    private IProductRepo iProductRepo;

    /*public void receiveMessage(String message) {
        System.out.println(message + "++receiveMessage");

        System.out.println(message + "--receiveMessage");
    }*/

    public void placeOrder(OrderDTO orderDTO) {
        System.out.println(orderDTO + "++OrderMessagesListener.placeOrder");
        iProductRepo.save(new Product(orderDTO));
        System.out.println(orderDTO + "--OrderMessagesListener.placeOrder");
    }


}
