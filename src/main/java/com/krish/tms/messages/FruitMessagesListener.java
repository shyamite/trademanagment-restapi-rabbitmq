package com.krish.tms.messages;

import org.springframework.stereotype.Component;

import com.krish.tms.dto.OrderDTO;

@Component
public class FruitMessagesListener {


    public void receiveMessage(OrderDTO orderDTO) {
        System.out.println(orderDTO + "++FruitMessagesListener.placeOrder"); 
    }


}
