package com.krish.tms.messages;

import com.krish.tms.entity.Ticket;
import com.krish.tms.repository.ITicketRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.krish.tms.dto.OrderDTO;
import com.krish.tms.entity.Product;
import com.krish.tms.repository.IProductRepo;

@Component
public class HomeApplianceMessagesListener {

    @Autowired
    private IProductRepo iProductRepo;


    @Autowired
    private ITicketRepo iTicketRepo;

    public void receiveMessage(Ticket ticket) {
        System.out.println(ticket + "++HomeApplianceMessagesListener.placeOrder");
        iTicketRepo.save(ticket);
       /* Product prd = new Product(orderDTO);
        iProductRepo.save(prd);*/
      //  orderDTO.setCategory(prd.getId()+"");
    }


}
