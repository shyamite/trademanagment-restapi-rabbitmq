package com.krish.tms.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class OrderDTO implements Serializable{

    private String name;
    private String description;
    private String category;
    private BigDecimal price;
    private String orderType;
    private String ticketId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    
    
    public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}


    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    @Override
	public String toString() {
		return "OrderDTO [name=" + name + ", description=" + description + ", category=" + category + ", price=" + price
				+ ", orderType=" + orderType + "]";
	}

 
}
