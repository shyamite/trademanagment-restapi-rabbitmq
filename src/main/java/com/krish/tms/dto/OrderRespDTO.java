package com.krish.tms.dto;

import com.krish.tms.entity.Ticket;

import java.math.BigDecimal;
import java.util.Date;

public class OrderRespDTO {

    private String name;
    private String description;
    private String category;
    private BigDecimal price;
    private String ticketNumber;
    private Date ticketCreationTime;

    public OrderRespDTO(Ticket ticket){

        ticketNumber =ticket.getTicketIdProduct();

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public Date getTicketCreationTime() {
        return ticketCreationTime;
    }

    public void setTicketCreationTime(Date ticketCreationTime) {
        this.ticketCreationTime = ticketCreationTime;
    }
}
