package com.krish.tms.repository;

import com.krish.tms.entity.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface ILoginRepo extends CrudRepository<User , Long>{

    public User findByEmailId(String emailId);


}