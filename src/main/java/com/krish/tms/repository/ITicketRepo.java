package com.krish.tms.repository;

import com.krish.tms.entity.Product;
import com.krish.tms.entity.Ticket;
import com.krish.tms.entity.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface ITicketRepo extends CrudRepository<Ticket, Long>{


}