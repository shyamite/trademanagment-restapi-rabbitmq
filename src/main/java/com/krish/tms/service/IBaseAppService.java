package com.krish.tms.service;

import java.util.List;

public interface IBaseAppService<T>{

    public T create(T t);
    public T get(T t);
    public T delete(T t);
    public T update(T t);
    public List<T> getAll(T t);
}
