package com.krish.tms.service;

import com.krish.tms.dto.UserDTO;

public interface IAuthenticationService  extends IBaseAppService<UserDTO>{
    public UserDTO validateUser(UserDTO user);



}
