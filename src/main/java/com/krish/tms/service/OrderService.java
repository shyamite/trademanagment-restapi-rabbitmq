package com.krish.tms.service;

import com.krish.tms.config.RabbitMQConfig;
import com.krish.tms.dto.OrderDTO;
import com.krish.tms.dto.OrderRespDTO;
import com.krish.tms.entity.Product;
import com.krish.tms.entity.Ticket;
import com.krish.tms.repository.IProductRepo;
import com.krish.tms.repository.ITicketRepo;
import com.krish.tms.utils.Constants;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class OrderService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private ITicketRepo iTicketRepo;

    @Autowired
    private IProductRepo iProductRepo;

    /* public void sendMessage(String msgOnQue){
         rabbitTemplate.convertAndSend(RabbitMQConfig.TMS_QUE_NAME , msgOnQue);
     }
 */
    public OrderRespDTO placeOrder(OrderDTO orderDTO) {
        Ticket ticket=null;
        Product product = iProductRepo.findByNameAndCategory(orderDTO.getName(), orderDTO.getCategory());
        //Product product = null;
        if (product == null) {
            product = new Product(orderDTO);
            iProductRepo.save(product);
        }
        if (orderDTO.getOrderType().equals(Constants.TMS_FRUIT_QUEUE)) {
            rabbitTemplate.convertAndSend(Constants.TMS_FRUIT_QUEUE, orderDTO);
        } else {
            ticket = new Ticket();
            String ticketID = ticket.getTicketId();
            //iTicketRepo.save(tt);
            //orderDTO.setTicketId(ticket.getTicketId());
            ticket.setProductId(product.getId());
            ticket.setTicketIdProduct(ticketID);
            rabbitTemplate.convertAndSend(Constants.TMS_HOME_APPLIANCE_QUEUE, ticket);
            System.out.println("Ticket No:" + ticket.getId());
        }

        return new OrderRespDTO(ticket);
    }

}
