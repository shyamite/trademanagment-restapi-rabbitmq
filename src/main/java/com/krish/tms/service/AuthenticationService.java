package com.krish.tms.service;

import com.krish.tms.dto.UserDTO;
import com.krish.tms.entity.Address;
import com.krish.tms.entity.User;
import com.krish.tms.repository.ILoginRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.xml.bind.helpers.AbstractUnmarshallerImpl;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class AuthenticationService implements IAuthenticationService {
    @Autowired
    private ILoginRepo iLoginRepo;

    @Override
    public UserDTO validateUser(UserDTO userdto) {

        User user = iLoginRepo.findByEmailId(new User(userdto).getEmailId());
        if(user == null){
            System.out.println("User is not found");
            return null;
        }
        else{
            System.out.println("User has been found");

            return userdto;
        }
    }

    @Override
    public UserDTO create(UserDTO userdto) {
        User user = new User(userdto);
        Address address = new Address( );
        address.setHouseNumber("50");
        address.setUser(user);
        Set<Address> addresses = new HashSet<Address>();
        addresses.add(address);

        user.setAddresses(addresses);
         iLoginRepo.save(user);
        System.out.println(user);
        return userdto;
    }

    @Override
    public UserDTO get(UserDTO userDTO) {
        return userDTO;
    }

    @Override
    public UserDTO delete(UserDTO userDTO) {
        return userDTO;
    }

    @Override
    public UserDTO update(UserDTO userDTO) {
        return userDTO;
    }

    @Override
    public List<UserDTO> getAll(UserDTO userDTO) {
        return null;
    }


}