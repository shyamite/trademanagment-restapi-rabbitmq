package com.krish.tms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages={"com.krish.tms.*"})
public class MainApp {

    //This is main app
    public static void main(String[] args) {
        SpringApplication.run(MainApp.class , args);
    }
   
}
